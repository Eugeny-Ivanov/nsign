<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%options}}`.
 */
class m200904_153300_create_options_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%options}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'state'=>$this->tinyInteger(1)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%options}}');
    }
}
