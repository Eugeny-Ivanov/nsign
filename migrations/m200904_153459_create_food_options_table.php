<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%food_options}}`.
 */
class m200904_153459_create_food_options_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%food_options}}', [
            'id' => $this->primaryKey(),
            'food_id'=>$this->integer(11)->notNull(),
            'option_id'=>$this->integer(11)->notNull(),
        ]);
        $this->createIndex(
            'food_id',
            '{{%food_options}}',
            'food_id'
        );
        $this->createIndex(
            'option_id',
            '{{%food_options}}',
            'option_id'
        );
        $this->addForeignKey(
            'food_options_ibfk_1',
            '{{%food_options}}',
            'food_id',
            '{{%foods}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'food_options_ibfk_2',
            '{{%food_options}}',
            'option_id',
            '{{%options}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'food_options_ibfk_1',
            '{{%food_options}}'
        );
        $this->dropIndex(
            'food_id',
            '{{%food_options}}'
        );
        $this->dropForeignKey(
            'food_options_ibfk_2',
            '{{%food_options}}'
        );
        $this->dropIndex(
            'option_id',
            '{{%food_options}}'
        );
        $this->dropTable('{{%food_options}}');
    }
}
