<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%foods}}`.
 */
class m200904_152739_create_foods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%foods}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'state'=>$this->tinyInteger(1)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%foods}}');
    }
}
