<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\nsign\models\FoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Foods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foods-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Foods', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['id'=>'grid-view'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'state',
                'format' => 'raw',
                'value' => function ($model){
                    if($model->state == 1){
                        return '<span class="label label-success">Yes</span>';
                    }else{
                        return '<span class="label label-danger">No</span>';
                    }
                },
                'contentOptions' => ['style'=>'padding:0px 0px 0px 30px;vertical-align: middle; text-align:center;'],
            ],
            [
                'label'=>'Ingridients',
                'value' => function ($model){
                   return join(', ',array_map(function ($el){return $el->name;},$model->options));
                },
                'contentOptions' => ['style'=>'padding:0px 0px 0px 30px;vertical-align: middle; text-align:center;'],
            ],
            'id',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
