<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\modules\nsign\models\Foods */
/* @var $form yii\widgets\ActiveForm */
use kartik\select2\Select2;
$model->option_ids = $model->options
?>

<div class="foods-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state')->checkbox() ?>

    <?= $form->field($model, 'option_ids')->widget(Select2::class, [
    'data' => ArrayHelper::map( \app\modules\nsign\models\Options::find()->all(),'id','name'),
    'options' => ['placeholder' => 'Select a state ...'],
    'pluginOptions' => [
    'allowClear' => true
    ],
        'options'=>[
            'multiple' => true,
            'placeholder' => 'Ингридиенты ...' ,
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
