<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\nsign\models\FoodsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="foods-search">
        <?php yii\widgets\Pjax::begin(['id' => 'search']) ?>
        <?php $form = ActiveForm::begin([
            'id' => 'filter-form',
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>

        <?= $form->field($model, 'options')->checkboxList(
            ArrayHelper::map(\app\modules\nsign\models\Options::find()->select(['id', 'name'])->where(['state' => 1])->asArray()->all(), 'id', 'name')
        )->label('Ингридиенты') ?>
        <div id="length" style="display: none; color: red">Выберите больше ингредиентов</div>
        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary', 'id' => 'reset']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <?php yii\widgets\Pjax::end() ?>
    </div>
<?php
$this->registerJs(<<<JS
//Фильтр по событию
$(document).on('change', '#foodssearch-options input', function(e) {
    if($('#foodssearch-options input:checked').length < 2){
        $('#length').css('display','block');
    }else{
        $('#length').css('display','none');
        var options =[];
        $.each($('#foodssearch-options input:checked'), function(index,value) {
            options.push($(value).val());        
        });        
        $.pjax({       
            timeout: 4000,
            url: $('#filter-form').attr('action'),
            container: '#grid-view',
            fragment: '#grid-view',
            data: {FoodsSearch: {options:options}},
        });
    }
});
//сброс фильтра
$(document).on('click', '#reset', function(e) {
    e.preventDefault();
    $.post( 
            $('#filter-form').attr('action'),
            {id:{}})
    .done(function( data ) {
            $('#SelectModel').html(data);
            $( "#filter-form input" ).prop( "checked", false );
            $.pjax({
                timeout: 4000,
                url: $('#filter-form').attr('action'),
                container: '#grid-view',
                fragment: '#grid-view',
                data: {}
            });
    });
});
JS
    , yii\web\View::POS_END);
