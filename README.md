yii2-nsign
==========
Test

Installation
------------
Установить yii2

В каталоге modules выполнить

```
git clone https://Eugeny-Ivanov@bitbucket.org/Eugeny-Ivanov/nsign.git
```
В конфиге добавить
````
'modules' => [
        'nsign' => [
            'class' => 'app\modules\nsign\Module',
        ],
    ],
````
Добавить в секцию require файла composer.json
````
"kartik-v/yii2-widget-select2": "*"
````
выполнить
````
composer update
````
настроить подключение к БД и выполнить
````
php yii migrate --migrationPath=@app/modules/nsign/migrations
````
Обратиться в модулю как обычно. Должно заработать

Оформлять как расширение не стал. Тест все же. Как бы ни к чему.
