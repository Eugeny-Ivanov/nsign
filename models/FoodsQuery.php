<?php

namespace app\modules\nsign\models;

/**
 * This is the ActiveQuery class for [[Foods]].
 *
 * @see Foods
 */
class FoodsQuery extends \yii\db\ActiveQuery
{
    /**
     * @return FoodsQuery
     */
    public function active()
    {
        $query = new \yii\db\Query();
        $notIn = $query->select(['fo.food_id'])
            ->from(['o'=>'options'])->innerJoin(['fo' => 'food_options'],'o.id=fo.option_id')
            ->where(['o.state'=>0]);
        return $this->joinWith(['options op'])
            ->select(['foods.*','COUNT(op.id) co'])
            ->andWhere(['NOT IN', 'foods.id', $notIn])
            ->groupBy(['foods.id'])
            ->orderBy(['co'=>SORT_DESC]);
    }

    /**
     * {@inheritdoc}
     * @return Foods[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Foods|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
