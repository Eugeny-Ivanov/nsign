<?php

namespace app\modules\nsign\models;

use Yii;

/**
 * This is the model class for table "options".
 *
 * @property int $id
 * @property string $name
 * @property int $state
 *
 * @property FoodOptions[] $foodOptions
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['state'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'state' => 'State',
        ];
    }

    /**
     * Gets query for [[FoodOptions]].
     *
     * @return \yii\db\ActiveQuery|FoodOptionsQuery
     */
    public function getFoodOptions()
    {
        return $this->hasMany(FoodOptions::className(), ['option_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return OptionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OptionsQuery(get_called_class());
    }
}
