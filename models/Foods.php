<?php

namespace app\modules\nsign\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "foods".
 *
 * @property int $id
 * @property string $name
 * @property int $state
 *
 * @property FoodOptions[] $foodOptions
 */
class Foods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $option_ids = [];

    public static function tableName()
    {
        return 'foods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','option_ids'], 'required'],
            [['state'], 'integer'],
            [['name'], 'string', 'max' => 100],
            ['option_ids', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'state' => 'State',
        ];
    }

    /**
     * Gets query for [[FoodOptions]].
     *
     * @return \yii\db\ActiveQuery|FoodOptionsQuery
     */
    public function getFoodOptions()
    {
        return $this->hasMany(FoodOptions::class, ['food_id' => 'id']);
    }

    public function getOptions()
    {
        return $this->hasMany(Options::class, ['id' => 'option_id'])->via('foodOptions');
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function saveOptions()
    {
        $old_option_ids = array_map(function ($el) {
            return $el->option_id;
        }, $this->foodOptions);
        $del = array_diff($old_option_ids, $this->option_ids);
        $insert = array_diff($this->option_ids, $old_option_ids);
        if(!$del && !$insert){
            return true;
        }
        if ($del) {
            FoodOptions::deleteAll(['and', ['food_id' => $this->id], ['in', 'option_id', $del]]);
        }
        $insert = array_filter($insert);
        if ($insert) {
            $fields = ['food_id', 'option_id'];
            $batch = array_map(function ($el) {
                return [$this->id, $el];
            }, $insert);
            if ($batch) {
                \Yii::$app->db->createCommand()->batchInsert(FoodOptions::tableName(), $fields, $batch)->execute();
            }
        }
        return true;
    }

    /**
     * {@inheritdoc}
     * @return FoodsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FoodsQuery(get_called_class());
    }
}
