<?php

namespace app\modules\nsign\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\nsign\models\Foods;

/**
 * FoodsSearch represents the model behind the search form of `app\modules\nsign\models\Foods`.
 */
class FoodsSearch extends Foods
{
    public $options;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['options', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Foods::find()->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'foods.id' => $this->id,
            'foods.state' => $this->state,
        ]);

        $query->andFilterWhere(['in', 'op.id', $this->options]);
        return $dataProvider;
    }
}
